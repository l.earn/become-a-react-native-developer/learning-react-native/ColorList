import React, { Component } from 'react';

import {
	View,
	Text,

	StyleSheet,

	TextInput
} from 'react-native';

import isColor from 'is-color';

export default class ColorForm extends Component {
	constructor() {
		super();

		this.state = {
			txtColor: ''
		}

		this.submit = this.submit.bind(this);
	}

	submit() {
		const enteredColor = this.state.txtColor.toLowerCase().trim();

		if(isColor(enteredColor)) {
			this.props.onNewColor(enteredColor);
		}

		this.setState({
			txtColor: '' 
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<TextInput 
					style={styles.txtInput} 
					placeholder='enter a color'
					onChangeText={(txtColor) => this.setState({ txtColor })}
					value={this.state.txtColor}
				/>
				<Text 
					style={[styles.button, { backgroundColor: this.props.currentColor }]}
					onPress={this.submit}>
					Add
				</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		height: 50
	},
	txtInput: {
		flex: 1,
		margin: 5,
		padding: 5,
		borderWidth: 2,
		borderRadius: 7,
		fontSize: 20,
		backgroundColor: 'white',
		color: 'black'
	},
	button: {
		margin: 5,
		padding: 5,
		borderWidth: 2,
		borderColor: 'white',
		alignSelf: 'stretch',
		backgroundColor: 'rgba(255,255,255,0.8)',
		color: 'white',
		fontSize: 20
	
	}
});