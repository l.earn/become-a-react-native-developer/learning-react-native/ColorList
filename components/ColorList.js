import React, { Component } from 'react';

import {
	Text,
	FlatList,
	View,

	StyleSheet,

	TouchableHighlight
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import ColorButton from './ColorButton';
import ColorForm from './ColorForm';

export default class ColorList extends Component {
	constructor() {
		super();

		const availableColors = [
			'#3DFAFF',
			'#43C59E',
			'#48BEFF',
			'#EFEA5A',
			'#B8E0D2',
			'#EC7357',
		];

		this.state = {
			availableColors
		};

		this.newColor = this.newColor.bind(this);
	}

	componentDidMount() {
		AsyncStorage.getItem(
			'@Learn:React:Native:ColorListStore:Colors',
			(err, data) => {
				if(err) {
					console.err('Error loading colors', err);
				} else {
					const availableColors = JSON.parse(data) || [];

					this.setState({
						availableColors 
					});
				}
			} 
		)
	}

	saveColors(colors) {
		AsyncStorage.setItem(
			'@Learn:React:Native:ColorListStore:Colors',
			JSON.stringify(colors)
		)
	}

	newColor(color) {
		const availableColors = [...this.state.availableColors, color];
		this.setState({
			availableColors
		});

		this.saveColors(availableColors);
	}

	render() {
		const { availableColors } = this.state;
		const { backgroundColor } = this.props;
		return(
			<FlatList style={[ styles.container, { backgroundColor } ]}
				data={ availableColors }
  				renderItem={({item}) => (<ColorButton backgroundColor={item} onSelect={this.props.onColorSelected} />)}
  				keyExtractor={item => item}
  				ListHeaderComponent={() => (
  					<View style={styles.header}>
  						<Text style={[styles.headerTxt, { color: backgroundColor }]}>Color List</Text>
  						<ColorForm currentColor={backgroundColor} onNewColor={this.newColor} />
  					</View>
  				)}
  				stickyHeaderIndices={[0]}>
			</FlatList>



		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	header: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'black',
		paddingTop: 30,
		padding: 10,
	},
	headerTxt: {
		fontSize: 30,
		textAlign: 'center',
		fontWeight: 'bold'
	}
});