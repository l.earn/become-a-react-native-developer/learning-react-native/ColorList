import React, { Component } from 'react';

import {
	TouchableHighlight,
	View,
	Text,
	StyleSheet
} from 'react-native';

const ColorButton = ({ backgroundColor, onSelect=f=>f }) => (
	<TouchableHighlight style={styles.button}
		onPress={() => onSelect(backgroundColor)}
		underlayColor='rgba(255,255,255,0.4)'>
		<View style={styles.row}>
			<View style={[ styles.preview, { backgroundColor } ]} />
			<Text style={[ styles.text, { color: backgroundColor } ]}>{backgroundColor}</Text>
		</View>
	</TouchableHighlight>
);

const styles = StyleSheet.create({
	button: {
		margin: 10,
		padding: 10,
		borderWidth: 2,
		borderRadius: 10,
		borderColor: 'white',
		alignSelf: 'stretch',
		backgroundColor: 'rgba(255,255,255,0.8)'
	},
	row: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	preview: {
		height: 20,
		width: 20,
		borderRadius: 10,
		margin: 20
	},
	text: {
		fontSize: 30,
	}
});

export default ColorButton;